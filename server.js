const schoolApi = require('./app_modules/schools-api');


const config = require('./config');


var express = require('express');
var net = require('net');
var myParser = require("body-parser");
var http = require('https');



var router = express.Router();
var app = express();

// consiguracion
app.use(myParser.json({
    extended: true
}));
app.use(myParser.urlencoded({
    extended: true
}));
app.use(router);
app.use('/colegiosbogota', express.static(__dirname + '/public'));
//app.use(express.favicon(__dirname + '/public/favicon.ico'));

app.use(function (req, res, next) {
    // res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});




console.log("INICIO DE SERVIDOR...............");



const PORT = config.server.PORT;

app.listen(PORT, () => {
    console.log("server init at : " + PORT);
    let schoolA = new schoolApi.SchoolsApi();

//    schoolA.getDisabilities().then((res)=>{
//        console.log(res);
//    }).catch((err) => {
//        console.log(err);
//
//    });

    /*
    schoolA.getCountFromOpenData().then((data) => {
        console.log("data");
        console.log(data);
    }).catch((err) => {
        console.log(err)
    });
    */


   // schoolA.initApp();

    // schoolA.getSchoolsByLocality(2);

    //schoolA.georeferenceBylocality(14)
    //  schoolA.getLocalities();

    /*
     schoolA.findSchools().then((result) => {
     console.log(result);
     }).catch((err) => {
     console.log(err);
     });
     */


//t.SchoolsApi();



});


app.post("/api/opendataschoolsbogota", function (request, response) {
    response.setHeader('Content-Type', 'application/json');

    execute(request.body).then((resp)=>{
        console.log("peticion exitosa");
        response.send(resp);
        response.end();
    }).catch((resp) =>{
        console.log("peticion no exitosa");
        response.send(resp);
        response.end();
    });




});

function execute (obj) {
    console.log(obj);

    return new Promise(function (resolve, reject) {
        try {
            if (obj) {
                if (obj.process) {
                    let schoolA = new schoolApi.SchoolsApi();
                    if (schoolA[obj.process]) {

                        schoolA[obj.process](obj.data).then((res)=>{
                            resolve(responseSuccess(res));
                        }).catch((err) => {
                            reject(responseError(err));
                        });

                    } else {
                        console.log("procceso no definido");
                        reject(responseError("procceso no definido"));
                    }
                } else {
                    reject(responseError("Proceso nulo"));
                }
            } else {
                reject(responseError("Objeto indefinido"));
            }
        }catch (e) {
            console.log(e);
            reject(responseError("Error de la aplicacion "));
        }
    });
};

function responseSuccess (data) {
    return {
        state :true,
        message : "success",
        data : JSON.stringify(data)
    }
}

function responseError (msg) {
    return {
        state :false,
        message : msg
    }
}






app.post("/getCountSchoolsBogota", function (request, response) {
    let schoolA = new schoolApi.SchoolsApi();
    response.setHeader('Content-Type', 'application/json');

    //let schoolA = new schoolApi.SchoolsApi();

    schoolA.getCountFromOpenData().then((data) => {
        console.log("data");
        console.log(data);
        response.send(data);
    response.end();
    }).catch((err) => {
        console.log(err);
        response.send("Error consultando datos");
        response.end();
    });


});


app.post("/getLocalities", function (request, response) {
    let schoolA = new schoolApi.SchoolsApi();
    response.setHeader('Content-Type', 'application/json');
    response.send(schoolA.getLocalities());
    response.end();
});

app.post("/getSchoolsByLocality", function (request, response) {
    let schoolA = new schoolApi.SchoolsApi();
    console.log(request.body);
    response.setHeader('Content-Type', 'application/json');

    schoolA.getSchoolsByLocality(request.body.id).then((result) => {
        response.send(result);
        response.end();
    }).catch((result) => {
        console.log(result);
        response.send("error");
        response.end();
    });
});







// app.post("/testjson", function (request, response) {
//     console.log(request.body);
//
//     var obj = {
//         data1: 45,
//         data2: "okpost"
//     };
//     var json = JSON.stringify(obj);
//
//     response.setHeader('Content-Type', 'application/json');
//     response.send(json);
//     response.end();
// });
//
//
// /* Captura de errores en el servidor  */
// process.on('uncaughtException', function (err) {
//     console.log('Caught exception: ' + err);
// });
