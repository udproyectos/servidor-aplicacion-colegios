
var map = null;
var circle = null;
var voidField = "--";
main.controller('admin-controller', function ($scope, $http, blockUI, $compile, ) {


    var markerCluster = null;

    var markers = [];

    $scope.dataGeocoding = {

    };

    $scope.DATA = [];


    $scope.latitudeGeocoding = null;
    $scope.longitudeGeocoding = null;

    $scope.rightBar = false;
    $scope.showEditBar = false;
    $scope.localitySelected = null;
    $scope.schoolSelected = {};
    $scope.geocodeActive = {
        state: false
    };




    $scope.dataTable = null;

    $scope.labels = [
        {data: "dane12", title: "Dane 12", readOnly : true},
        {data: "name", title: "Nombre", readOnly : false},
        {data: "name_sede", title: "Nombre sede", readOnly : false},
        {data: "address_", title: "Dirección Original", readOnly : false},
        {data: "address_google", title: "Dirección Google", readOnly : false},
        {data: "latitude", title: "Latitud", readOnly : false},
        {data: "longitude", title: "Longitud", readOnly : false},
        {data: "state", title: "Estado", readOnly : false},
        {data: "edit", title: "Ubicar En Mapa", readOnly : false}
    ];



    $scope.d;//pendiente





    markerEdit = null
    $scope.init = function () {

        $scope.getLocalities();

        let mapCanvas = document.getElementById("map");
        let myCenter = new google.maps.LatLng(4.624335, -74.063644);
        let mapOptions = {
            center: myCenter,
            zoom: 11
        }

        map = new google.maps.Map(mapCanvas, mapOptions);

        google.maps.event.addListener(map, 'click', function (event) {

            if (circle !== null) {
                circle.setMap(null);
            }

            if ($scope.geocodeActive.state) {
                console.log("Latitude: " + event.latLng.lat() + " " + ", longitude: " + event.latLng.lng());
                let geocoder = new google.maps.Geocoder;

                if (markerEdit != null) {
                    markerEdit.setMap(null)
                }



                markerEdit = new google.maps.Marker({
                    position: event.latLng,
                    map: map,
                    icon: './images/icons/newLocation.png'
                });


                geocoder.geocode({'location': event.latLng}, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {

                            $scope.schoolSelected.address_google = results[0].formatted_address;
                            $scope.schoolSelected.latitude = event.latLng.lat();
                            $scope.schoolSelected.longitude = event.latLng.lng();
                            $scope.schoolSelected.marker.setPosition(event.latLng);
                            $scope.$apply();

                            console.log(results[0].formatted_address);

                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
            }
        });
    };


    $scope.geocode = function () {
        console.log($scope.dataGeocoding);
        let geocodert = new google.maps.Geocoder;
        geocodert.geocode({'address': $scope.dataGeocoding.addressToGeocode}, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {

                    $scope.schoolSelected.address_google = results[0].formatted_address;
                    $scope.schoolSelected.latitude = results[0].geometry.location.lat();
                    $scope.schoolSelected.longitude = results[0].geometry.location.lng();
                   

                    $scope.schoolSelected.marker.setPosition(results[0].geometry.location);
                    map.panTo(results[0].geometry.location);
                    map.setZoom(18);

//                    $scope.dataGeocoding.addressFoundGeocode = results[0].formatted_address;

                    if (markerEdit != null) {
                        markerEdit.setMap(null)
                    }

                    markerEdit = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map,
                        icon: './images/icons/newLocation.png'
                    });

                    console.log(results);

                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });

    };



    $scope.editMapMarkerSchool = function () {
        if (markerEdit != null) {
            markerEdit.setMap(null);
        }
    };



    $scope.syncLocalitySchoolsGeocoder = function (id) {
        console.log(id)
        console.log($scope.localitySelected)

        send("georeferenceByLocality", {id: id}, function (res) {
            console.log(res.data);
            $('#syncModal').modal('hide');
            //alert(res.data);

        });
    }


    $scope.getLocalities = function () {
        send("getDataSimple", JSON.stringify({'key' : 'LOCALITIES'}), function (res) {
            console.log(res.data);
            $scope.localidades =  JSON.parse(res.data)  ;
        });
        /*
         send("getCountFromOpenData", null, function (res) {
         console.log(res.data);
         $scope.d = res.data;
         });
         
         */

    }



    $scope.getSchoolsByLocality = function (locality) {
        $scope.localitySelected = locality;
        send("getSchoolsByLocality", {id: locality.id}, function (res) {
            $scope.clearMarkers();
//            let dataT = [];
            $scope.DATA = JSON.parse(res.data) ;

            $scope.chargeDataTable();

            for (let d of $scope.DATA) {

                markers.push($scope.createMarker(d));

            }



            if (markerCluster != null) {
                markerCluster.clearMarkers();
                markerCluster = null;
            }

            markerCluster = new MarkerClusterer(map, markers,
                    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
                        maxZoom: 15});




        });
    };
    
    
    $scope.updateSchool = function () {
        $scope.chargeDataTable();
        $scope.editMapMarkerSchool();
        console.log("guardando..");
        console.log($scope.schoolSelected);
        $('#updateModal').modal('hide');
        $scope.showEditBar = false;
        $scope.geocodeActive.state = false;

        let arrayKeys = [];
        for (let i of $scope.labels) {
            arrayKeys.push(i.data);
        }

        let data = $scope.schoolSelected;
        data.marker = null;



        send ("updateSchool",data, function (res) {
            console.log(res);
            $scope.schoolSelected = null;
            alert(res.data);
        });

    };


    $scope.chargeDataTable = function () {
        console.log("cargando tabla");
        if ($scope.dataTable) {
            $scope.dataTable.destroy();
            $scope.dataTable = null;
        }

        let dataT = [];


        for (let d of $scope.DATA) {

            let o = {};
            for (let k of $scope.labels) {

                if (k.data === 'state') {
                    o[k.data] = (d[k.data] === 0) ? 'Inactivo' : 'Activo';
                } else {
                    o[k.data] = (d[k.data]) ? d[k.data] : voidField;
                }

            }

            o.edit = "<button onclick=\"locateFromTable('" + o.latitude + "', '" + o.longitude + "')\"  type=\"button\" class=\"btn btn-secondary\" >Ubicar</button>";

            dataT.push(o);

        }

        $scope.dataTable = $('#geoTable').DataTable({
            data: dataT,
            columns: $scope.labels
        });
    };





    send = function (process, data, callback) {
        blockUI.start();
        $http({
            method: "POST",
            url: "/api/opendataschoolsbogota",
            data: {
                process: process,
                data: data
            }
        }).then(function mySuccess(response) {
            console.log(response.data.state);
            console.log(response.data.message);
            callback(response.data)
            blockUI.stop();
        }, function myError(response) {
            
            console.log(response);
            blockUI.stop();
        });
    };


    $scope.setLocalitySelected = function (localidad) {
        $scope.localitySelected = localidad;
    };

    $scope.clearMarkers = function () {
        for (let marker of markers) {
            marker.setMap(null);
        }
        markers = [];
    };




    $scope.createMarker = function (data) {

        data.state = (data.state == 1) ? true : false;

        let loc = {
            lat: data.loc.coordinates[1],
            lng: data.loc.coordinates[0]
        }

        let marker = new google.maps.Marker({
            position: loc,
            map: map,
            title: data.name,
            icon: './images/icons/school.png',

        });

        marker.addListener('dblclick', function () {
            $scope.showEditBar = true;
            $scope.schoolSelected = data;
            $scope.schoolSelected.marker = marker;
            $scope.schoolSelected.latitude = data.loc.coordinates[1];
            $scope.schoolSelected.longitude = data.loc.coordinates[0];
      //      $scope.schoolSelected.state = ($scope.schoolSelected.state === 1) ? true : false;
            $scope.$apply();
            console.log($scope.schoolSelected);
            // marker.setDraggable(true);
        });

        return marker;


    };



});

function locateFromTable(lat, lng) {
    
    if (lat === voidField || lng === voidField) {
        console.log("No se puede georeferenciar");
    } else {
        
        
        var lng = new google.maps.LatLng(lat, lng);
        map.panTo(lng);
        map.setZoom(18);

        if (circle !== null) {
            circle.setMap(null);
        }

        circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: lng,
            radius: 100
        });

        google.maps.event.addListener(circle, 'click', function (ev) {
            circle.setMap(null);
        });
        
    }

}