var main = angular.module('main',  ['ngRoute','blockUI']);


main.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/app/colegios', {
            templateUrl : 'app/admin/view.html',
            controller  : 'admin-controller'
        }).otherwise({
        redirectTo: '/app/colegios'
    });
}]);

angular.module('main').component("header", {
    templateUrl: "components/body/header.html"
});

angular.module('main').component("footer", {
    templateUrl: "components/body/footer.html"
});