
module.exports.localities = [
    {"id" : 1, "name" : "Usaquén"},
    {"id" : 2, "name" : "Chapinero"},
    {"id" : 3, "name" : "Santa Fe"},
    {"id" : 4, "name" : "San Cristóbal"},
    {"id" : 5, "name" : "Usme"},
    {"id" : 6, "name" : "Tunjuelito"},
    {"id" : 7, "name" : "Bosa"},
    {"id" : 8, "name" : "Kennedy"},
    {"id" : 9, "name" : "Fontibón"},
    {"id" : 10, "name" : "Engativá"},
    {"id" : 11, "name" : "Suba"},
    {"id" : 12, "name" : "Barrios Unidos"},
    {"id" : 13, "name" : "Teusaquillo"},
    {"id" : 14, "name" : "Los Mártires"},
    {"id" : 15, "name" : "Antonio Nariño"},
    {"id" : 16, "name" : "Puente Aranda"},
    {"id" : 17, "name" : "La Candelaria"},
    {"id" : 18, "name" : "Rafael Uribe Uribe"},
    {"id" : 19, "name" : "Ciudad Bolívar"},
    {"id" : 20, "name" : "Sumapaz"},

];
