const NodeGeocoder = require('node-geocoder');
const config = require.main.require('./config');


const geocoder_ = NodeGeocoder(config.google_options);



module.exports.Geocoder = class Geocoder {
    constructor(address) {
        this.address = address;
    };

    calculate() {
        console.log("geocoding.." + this.address);
        console.log(this.address);
        let a = this.address;
        return new Promise(function (resolve, reject) {
            geocoder_.geocode(a)
                .then(function (res) {
                    console.log(res)
                    let d = {
                        latitude:(res[0]) ? res[0].latitude : 0,
                        longitude: (res[0]) ? res[0].longitude : 0,
                        address: (res[0]) ? res[0].formattedAddress : 0,
                    };
                    resolve(d);

                }).catch(function (err) {
                console.log("Error: " + err.message);
                reject(err);
            });
        });
    };
};

