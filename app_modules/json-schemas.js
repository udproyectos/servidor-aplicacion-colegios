

module.exports.schoolsSch = {
    $jsonSchema: {
        bsonType: "object",
        required: [
            "locality",
            "locality_name",
            "neighborhood",
            "address_",
            "name",
            "email",
            "phone",
            "gender",
            "dane12",
            "calendar",
            "caracter_para_la_media",
            "class_",
            "disability",
            "especialidad_para_la_media",
            "state"
            ],
        properties:{
            locality: {
                bsonType: "string",
                description: "must be a string and is required"
            },
            locality_name: {
                bsonType: "string",
                description: "must be a string and is required"
            },
            neighborhood: {
                bsonType: "string",
                description: "must be a string and is required"
            },
            address_: {
                bsonType: "string",
                description: "must be a string and is required"
            },
            name: {
                bsonType: "string",
                description: "must be a string and is required"
            },
            email: {
                bsonType: "string",
                description: "must be a string and is required"
            },
            phone: {
                bsonType: "string",
                description: "must be a string and is required"
            },
            gender: {
                bsonType: "string",
                description: "must be a string and is required"
            },
            dane12 : {
                bsonType: "string",
                description: "must be a string and is required"
            },
            calendar : {
                bsonType: "string",
                description: "must be a string and is required"
            },
            caracter_para_la_media : {
                bsonType: "string",
                description: "must be a string and is required"
            },
            class_ : {
                bsonType: "string",
                description: "must be a string and is required"
            },
            disability : {
                bsonType: "string",
                description: "must be a string and is required"
            },
            especialidad_para_la_media : {
                bsonType: "string",
                description: "must be a string and is required"
            },
            loc : {
                bsonType: "object",
                description: "must be a object and is required"
            },
            state : {
                bsonType: "number",
                description: "must be a object and is required"
            }
        }
    }

};
