const OpenData = require('../app_modules/open-data-connection');
const GoogleApis = require('../app_modules/google-apis');
const db = require('../app_modules/mongodb-connection');
const resources = require('../json-resources');




function getAddress(i){

    if (i.direccion1_georeferenciacion === undefined) {
        if (i.direccion2_comun_reporte_colegios === undefined) {
            console.log("error en la direccion del colegio " );
            console.log(i);
        } else {
            return i.direccion2_comun_reporte_colegios.replace('#',' ').replace('-',' ');
        }
    } else {
        return i.direccion1_georeferenciacion.replace('#',' ').replace('-',' ');
    }


}


module.exports.SchoolsApi = class SchoolsApi {

    constructor () {
        this.mongoCon = new db.MongoDB();
        this.schoolsCollection = "testschema";
    };


    //discapacidades, localidades, especialidad para la media
    getDataSimple (data) {
        let jData = JSON.parse(data);
        let mongoCon = this.mongoCon;

        console.log("Get data " + JSON.stringify(jData));
        console.log("Get data " + JSON.stringify(jData.key));

        return new Promise(function (resolve, reject) {

            switch (jData.key) {
                case "SPECIALITIES":
                    console.log("SPECIALIZATION get");
                    mongoCon.findAll(mongoCon.options.academicColl, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                    break;
                case "DISABILITIES":
                    console.log("disabilities get");
                    mongoCon.findAll(mongoCon.options.disabilityColl, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                    break;
                case "LOCALITIES":
                    console.log("localities get");
                    if (resources.localities) {
                        resolve(resources.localities);
                    } else {
                        reject("no hay datos");
                    }
                    break;
                default :
                    console.log("error : tipo de consulta no encontrada")
                    reject("error : tipo de consulta no encontrada");
                    break;
            }
        });
    }



    // getDisabilities () {
    //     console.log("Obtiendo discapacidades");
    //     let mongoCon = this.mongoCon;
    //     return new Promise(function (resolve, reject) {
    //         mongoCon.findAll(mongoCon.options.disabilityColl, (err, result) => {
    //             if (err) {
    //                 reject(err);
    //             } else {
    //                 resolve(result);
    //             }
    //         });
    //
    //     });
    // };

    // getSpecialization () {
    //     console.log("Busacando localidades...");
    //     // console.log(resources.localities);
    //     return new Promise(function (resolve, reject) {
    //         if (resources.localities) {
    //             resolve(resources.localities);
    //         } else {
    //             reject("no hay datos")
    //         }
    //     });
    // }



    // getLocalities () {
    //     console.log("Busacando localidades...");
    //    // console.log(resources.localities);
    //     return new Promise(function (resolve, reject) {
    //         if (resources.localities) {
    //             resolve(resources.localities);
    //         } else {
    //             reject("no hay datos")
    //         }
    //     });
    // };

    getSchoolsByFilters (data) {
        console.log("----------");
        console.log(data);
        let jsonData = JSON.parse(data);
        let mongoCon = this.mongoCon;
        return new Promise(function (resolve, reject) {

            mongoCon.findGeoJson(jsonData, (err, result) => {
               if (err) {
                   console.log(err);
                   reject(err);
               } else {
                   for (let school of result) {
                       school.latitude = school.loc.coordinates[1];
                       school.longitude = school.loc.coordinates[0];
                       delete school.loc;
                   }

                   console.log(result.length);
                   resolve(result)
               }
            });

        });
    }
    
    
    // getSchoolsFiltered (data) {
    //     console.log("Obtiendo colegios por filtros " + data);
    //     let mongoCon = this.mongoCon;
    //
    //     return new Promise(function (resolve, reject) {
    //         let dataJSON = JSON.parse(data);
    //         console.log(dataJSON);
    //
    //         let obj = {
    //           disability: { '$regex': dataJSON.disability },
    //           locality : dataJSON.locality
    //         };
    //
    //         console.log("filter " + obj);
    //         console.log(obj);
    //
    //
    //         mongoCon.findByObject(mongoCon.options.collection, obj, (err, result) => {
    //             if (err) {
    //                 console.log(err);
    //                 reject(err);
    //             } else {
    //
    //                 for (let school of result) {
    //                     school.latitude = school.loc.coordinates[1];
    //                     school.longitude = school.loc.coordinates[0];
    //                     delete school.loc;
    //                 }
    //
    //                 console.log(result.length);
    //                 resolve(result);
    //             }
    //         });
    //     });
    // }
    

    getSchoolsByLocality (data) {
        console.log("obteniendo colegios por localidad " + data.id);
        let mongoCon = this.mongoCon;
        return new Promise(function (resolve, reject) {
            mongoCon.findBy(mongoCon.options.collection,"locality", data.id+"", (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    console.log("colegios encontrados"+ result.length);
               
                    for (let d of result) {
                        console.log(d);
                        d.latitude = (d.loc.coordinates[1]) ? d.loc.coordinates[1] : "--" ;
                        d.longitude = (d.loc.coordinates[0]) ? d.loc.coordinates[0] : "--";
                    }
                    resolve(result);
                }
            });
        });
    };

    updateSchool (data) {
        console.log("Actualizando colegio");
//        console.log(data);
        let mongoCon = this.mongoCon;
        
        let values = {
            loc : {
                type : "Point",
                coordinates : [data.latitude+0, data.longitude+0]
            },
            name : data.name,
            name_sede : data.name_sede,
            address_google : data.address_google,
            locality : data.locality,
            locality_name : data.locality_name,
            neighborhood : data.neighborhood,
            address_ :data.address_,
            email : data.email,
            phone :data.phone,
            gender : data.gender,
            dane12 : data.dane12,
            calendar : data.calendar,
            caracter_para_la_media :data.caracter_para_la_media,
            class_ : data.class_,
            disability : data.disability,
            especialidad_para_la_media :data.especialidad_para_la_media,
            state : (data.state) ? 1 : 0
            
            
        };
        console.log(values);

        return new Promise(function (resolve, reject) {
            mongoCon.updateOne(mongoCon.options.collection, {dane12 : data.dane12}, {$set : values}, function (err, res) {
                if (err) {
                    console.log(err);
                    reject("Error actualizando");
                }
                //console.log(res);
                resolve("colegio actualizado");
            });



        })
    }



//    findSchools () {
//        console.log("Busacando...");
//        let mongoCon = this.mongoCon;
//        return new Promise(function (resolve, reject) {
//            mongoCon.findAll("testschema", (err, result) => {
//                if (err) {
//                    reject(err);
//                } else {
//                    resolve(result);
//                }
//            });
//        });
//    }


    getVal(o){
        return (o) ? o : "NN";
    }


    getCountFromOpenData(){
        console.log("count data ....");
        let mongoCon = this.mongoCon;
        return new Promise(function (resolve, reject) {

            mongoCon.count(mongoCon.options.collection, function (err, data) {
                let resp = {};
                if (err) {
                    reject("No se encontraron registros en base de datos");
                } else {
                    resp.dbDCount = data;
                    OpenData.getCountSchoolsBogota().then((data) => {
                        resp.openDCount = data[0].count;
                        resolve(resp);
                    }).catch((err) => {
                       console.log(err);
                       reject("problemas contando datos de open data"+err)

                    });
                }

            });

        });

    };






    initApp () {
        console.log("Descargando datos de colegios ");
        let mongoCon = this.mongoCon;
        mongoCon.init();
        let data = OpenData.getSchoolsBogota();
        let array = [];
        var disabilitys = new Set();
        var academic = new Set();

        data.then((d) => {
            console.log("Consultando colegios : ____ datos encontrados :" + d.length);
            console.log(d[0])
            for (let i of d) {
                if (i !== undefined) {
                    let o = {
                        locality : this.getVal(i._loc),
                        locality_name : this.getVal(i.nombre_localidad),
                        neighborhood : this.getVal(i.barrio1_geo),
                        address_ : (i.direccion1_georeferenciacion) ? this.getVal(i.direccion1_georeferenciacion)
                            : this.getVal(i.direccion2_comun_reporte_colegios),
                        name : this.getVal(i.nombre_establecimiento_educativo),
                        name_sede : this.getVal(i.nombre_sede_educativa),
                        zip_code : this.getVal(i.codigo_postal),
                        email : this.getVal(i.email),
                        phone : this.getVal(i.telefono),
                        gender : this.getVal(i.genero),
                        dane12 : this.getVal(i.dane12_sede_educativa),
                        calendar : this.getVal(i.calendario),
                        caracter_para_la_media : this.getVal(i.caracter_para_la_media),
                        class_ : this.getVal(i.clase),
                        disability : this.getVal(i.discapacidad_por_categoria),
                        especialidad_para_la_media : this.getVal(i.especialidad_para_la_media),
                        loc : {
                            type : "Point",
                            coordinates: [0, 0]
                        },
                        state : 0

                    };

                    for (let d of this.getVal(i.especialidad_para_la_media).split("-")) {
                        academic.add(d.trim());
                    }

                    for (let d of this.getVal(i.discapacidad_por_categoria).split("-")) {
                        disabilitys.add(d.trim());
                    }

                    array.push(o);
                }
            }

            console.log("array " + array.length);
            console.log(disabilitys);
            console.log(academic);
            console.log(mongoCon.options.disabilityColl+'')

            // guardar coleccion de discapacidades
            for (let d of disabilitys) {
                let o = {
                    disability_name : d
                }
               // mongoCon.insertOne(mongoCon.options.disabilityColl, o);
            }

            // guardar las especialidades academicas
            for (let ea of academic) {
                let o = {
                    specialization_name :  ea
                }
                mongoCon.insertOne(mongoCon.options.academicColl, o);
            }

           // mongoCon.insertMany(mongoCon.options.collection, array);

        }).catch((err) => {
            console.log("errr");
            console.log(err)
        });


    }


    georeferenceByLocality (obj) {
        console.log("init sync " + obj );
        console.log(obj );
        let inst =  this;

        return new Promise(function (resolve, reject) {

            try {
                inst.getSchoolsByLocality(obj).then((data) => {

                    if (data.length === 0) {
                        resolve("No se actualizaron datos");
                    } else {

                        const filtered = data.filter( o =>  o.loc.coordinates[0] === 0 );

                        console.log(filtered.length);

                        inst.geocodeUpdateValues(filtered, function () {
                            console.log("Finalizo actualizacion de colegios localidad " + obj.id);

                        });

                        resolve({
                            time : (filtered.length*5)/60,
                        });



                    }

                }).catch((err) => {
                    console.log(err);
                    reject(err);
                });

            } catch (e) {
                console.log(e);
                reject("error en la aplicacion")
            }
        });
    }


    geocodeUpdateValues (array, callback) {
        let inst = this;
        let mongoCon = this.mongoCon;
        if (array.length !== 0) {
            let o = array.shift();



            let geo = new GoogleApis.Geocoder({address: o.address_ + ", Bogota", country: 'CO'});

            geo.calculate().then((res) => {
                if (res) {
                 //   console.log(res.address)
                    let newValues = {
                        $set:{
                            loc : {
                                type : "Point",
                                coordinates: [res.longitude, res.latitude]
                            },
                            address_google : res.address
                        }
                    };
                    let query = {dane12: o.dane12};


                    mongoCon.updateOne(mongoCon.options.collection, query, newValues, function (err, res) {
                         if (err) {
                            console.log(err);

                        }
                        inst.geocodeUpdateValues(array, callback);
                    });
                } else {
                    console.log("indefinido ... " + o.address_ + " Bogota, Colombia" );
                    inst.geocodeUpdateValues(array, callback);
                }
            }).catch((err) => {
                console.log(err);
                inst.geocodeUpdateValues(array, callback);
            });
        } else {
            callback();
        }
    }





};


