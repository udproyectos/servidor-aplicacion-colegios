const MongoClient = require('mongodb').MongoClient;
const config = require.main.require('./config');
const schemas = require('../app_modules/json-schemas');




module.exports.MongoDB = class MongoDB {

    constructor () {
        this.options = config.mongo_;
    };

  createCollection (collection) {

      MongoClient.connect(this.options.uri, function(err, client) {
          if (err) throw err;
          var dbo = client.db(this.options.database);

          dbo.createCollection(collection, function(err, res) {
              if (err) throw err;
              console.log("Collection created!");
              client.close();
          });

      });
  };

  insertOne (collection, object) {
      console.log(collection);
      console.log("insert : " + JSON.stringify(object) );
      let options = this.options;
      MongoClient.connect(options.uri, function(err, client) {
          if (err) throw err;
          var dbo = client.db(options.database);

          dbo.collection(collection).insertOne(object, function(err, res) {
            //  console.log(err);
              if (err) throw err;
              console.log("1 document inserted");
              // console.log(res);
              client.close();
          });

      });
  };

  updateOne (collection,query,newValues, callback) {
      let options = this.options;
      MongoClient.connect(options.uri, function(err, client) {
          if (err) throw err;
          var dbo = client.db(options.database);

          dbo.collection(collection).updateOne(query, newValues, function(err, res) {

              console.log("1 document updated");
              callback(err, res);
              client.close();
          });
      });
  }
  
  insertMany (collection, objects) {
      let options = this.options;
      MongoClient.connect(options.uri, function(err, client) {
          if (err) throw err;
          var dbo = client.db(options.database);

          dbo.collection(collection).insertMany(objects, function(err, res) {
              if (err) throw err;
              console.log("Number of documents inserted: " + res.insertedCount);
             // console.log("1 document inserted");
              // console.log(res);
              client.close();
          });

      });
  }
  
  findByObject (collection, object, callback) {
      let dataBase = this.options.database;

      MongoClient.connect(this.options.uri, function(err, client) {
          if (err) throw err;
          var dbo = client.db(dataBase);
         
          dbo.collection(collection).find(object).toArray(function(err, result) {
              client.close();
              callback(err, result);
              /*
              if (err) throw err;
              console.log(result);
              db.close();
              */
          });
      });
  }

  findBy (collection, field, value, callback) {
      let dataBase = this.options.database;

      MongoClient.connect(this.options.uri, function(err, client) {
          if (err) throw err;
          var dbo = client.db(dataBase);
          let filter = {};
          filter[field] = value;


          dbo.collection(collection).find(filter).toArray(function(err, result) {
              client.close();
              callback(err, result);
              /*
              if (err) throw err;
              console.log(result);
              db.close();
              */
          });
      });
  }

  findAll (collection, callback) {
      console.log(this.options);
      let dataBase = this.options.database;

      MongoClient.connect(this.options.uri, function(err, client) {
          if (err) throw err;
          var dbo = client.db(dataBase);

          dbo.collection(collection).find({}).toArray(function(err, result) {
              client.close();
              callback(err, result);
              /*
              if (err) throw err;
              console.log(result);
              db.close();
              */
          });
      });
  };

//   findDistinct (collection, row, callback) {
//       let dataBase = this.options.database;
//       MongoClient.connect(this.options.uri, function(err, client) {
//           if (err) throw err;
//           let dbo = client.db(dataBase);
//
//           dbo.collection(collection).find().forEach((d) => {
//               console.log(d)
//              // callback(d);
//           })
// /*
//           dbo.collection(collection).distinct(row,{}, function(err, result) {
//               client.close();
//               callback(err, result);
//           });
//           */
//       });
//
//   };



  findGeoJson(data, callback){
      let dataBase = this.options.database;
      let collection = this.options.collection;
      console.log(collection);
      console.log(data);
      MongoClient.connect(this.options.uri, function(err, client) {
          if (err) throw err;
          let dbo = client.db(dataBase);

          let filter = {};

          if (data.speciality) {
              filter.especialidad_para_la_media = data.speciality;
          }

          if (data.disability) {
              filter.disability = data.disability;
          }
          if (data.locality) {
              filter.locality = data.locality;
          }
          if (data.radius != 0) {
              filter.loc =  { $nearSphere:
                      {
                          $geometry: { type: "Point",  coordinates: [  data.longitude, data.latitude ] },
                          $maxDistance: data.radius
                      }
              }
          }





          console.log(JSON.stringify(filter));

          dbo.collection(collection).find(filter).toArray(function(err, result) {
              client.close();
              callback(err, result);
              /*
              if (err) throw err;
              console.log(result);
              db.close();
              */
          });


      });


  }





  count(collection, callback) {
      console.log("count data from collection");
      let dataBase = this.options.database;
      MongoClient.connect(this.options.uri, function(err, client) {
          if (err) throw err;
          var dbo = client.db(dataBase);

          dbo.collection(collection).count(function (err, result) {
              console.log("count : " + result);
              client.close();
              callback(err, result);
          })
      });
  }

  init() {
      console.log("init db");
      MongoClient.connect(config.mongo_.uri, function (err, client) {
          if (err) throw err;
          let dbo = client.db(config.mongo_.database);

          dbo.createCollection(config.mongo_.collection, {validator: schemas.schoolsSch}, function (err, res) {
              if (err) throw err;
              console.log("init Collection created! " + config.mongo_.collection + " en " + config.mongo_.database);

              dbo.collection(config.mongo_.collection).createIndex({ "dane12": 1 }, { unique: true } , function (err, res) {
                  if (err) throw err;
                  console.log("index loc creado");
                  console.log(res);
              });
              dbo.collection(config.mongo_.collection).createIndex({loc: "2dsphere"}, function (err, res) {
                  if (err) throw err;
                  console.log("index loc creado");
                  console.log(res);
              });

              dbo.collection(config.mongo_.disabilityColl).createIndex({ "disability_name": 1 }, { unique: true } , function (err, res) {
                  if (err) throw err;
                  console.log("index disability_name creado");
                  console.log(res);
              });

              dbo.collection(config.mongo_.academicColl).createIndex({ "specialization_name": 1 }, { unique: true } , function (err, res) {
                  if (err) throw err;
                  console.log("index disability_name creado");
                  console.log(res);
              });
          })
      });

  }






};
