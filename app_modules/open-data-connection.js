const http = require('https');
const config = require.main.require('./config');



function getSchoolsBogota () {

    return new Promise(function(resolve, reject) {

        http.get(config.http_url._schools_bogota + '$limit=8000', (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
                data += chunk;
            });

            resp.on('end', () => {
                resolve(JSON.parse(data));
            });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
            reject(err);
        });

    });
};

function getCountSchoolsBogota () {
    return new Promise(function(resolve, reject) {

        http.get('https://www.datos.gov.co/resource/3cfk-twzy.json?$select=count(*)', (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
                data += chunk;
            });

            resp.on('end', () => {
                resolve(JSON.parse(data));
            });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
            reject(err);
        });

    });
};

module.exports.getSchoolsBogota =  getSchoolsBogota;
module.exports.getCountSchoolsBogota =  getCountSchoolsBogota;